# The test key needs to be at least 32 characters for testing
# encryption and decryption.
SECRET_KEY = 'test-key-test-key-test-key-test-key-test-key'
ROOT_URLCONF = 'django_scim.urls'

HTTP_AUTH_REALM = 'localhost'

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django_scim'
]

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'tests.hashers.OldSHA1PasswordHasher'
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'django_scim',
        'USER': 'bitbucket',
        'PASSWORD': 'bitbucket',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

MIDDLEWARE_CLASSES = []
